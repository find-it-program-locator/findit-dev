# Find It Program Locator and Event Discovery platform README

This is a composer based installer and virtual machine setup for developers working on the [Find It Program Locator distribution](https://gitlab.com/find-it-program-locator/).

## Installation

### Prerequisites

Docker and DDEV: https://ddev.readthedocs.io/en/stable/#installation

```
git clone git@gitlab.com:find-it-program-locator/findit-dev.git
cd findit-dev
# Substitute your specific site's configuration in this next line
git clone git@gitlab.com:find-it-program-locator/find-it-cambridge-config.git config
ddev start
ddev auth ssh
```

## Bringing down content from live replacing anything you have locally

This will **wipe out your local database and uncommitted configuration (and possibly code)** and replace it with the content on live and the latest code, updating the database as this code requires:

```
git pull
ddev auth ssh
ddev composer update
ddev composer pull
ddev drush cr
ddev drush updb -y
cd config/ && git pull && cd -
ddev drush cim -y
```

This makes your local live-like, or rather, like live if you were to deploy to live with the current state of code and configuration.

## Destructively update your development environment

**Wipe out your local database and un-Featurized configuration** and re-install the distribution with:

```
ddev composer fresh-start
```

Breaking that down what those two commands do...

**Potentially wipe out unpushed local code and exported features** and **update to the latest code changes others have made** to [Find It profile](https://gitlab.com/find-it-program-locator/findit), [Find It Organization](https://gitlab.com/find-it-program-locator/drutopia_findit_organization), [Find It Program](https://gitlab.com/find-it-program-locator/drutopia_findit_program), etc with a Composer update:

`composer fresh-start` is an alias for:

```
drush -y si findit && drush -y en drutopia_dev_findit && drush uli
drush locale:import en modules/contrib/findit_interface_text/overrides/custom-translations.en.po --type=customized --override=all
```

## Updating local Solr configuration

The local ddev instance includes a Solr instance. The configuration is controlled by an extra docker compose file in the .ddev folder, docker-compose.solr.yaml. This configuration uses the configuration inside of .ddev/solr/conf to configure the Solr instance to provide a core called `Drupal`

If extranous files are in the Solr container, you can remove the entire volume. For example `ddev stop && docker volume rm ddev-findit-dev_solr`

## Custom interface translations

Export manually at `/admin/config/regional/translate/export` until [pull request 3253 add command to export .po files](https://github.com/drush-ops/drush/pull/3253) is merged into Drush.

Then add (and commit) the strings you customized to the `findit_interface_text` module's [`overrides/custom-translations.en.po` file](https://gitlab.com/find-it-program-locator/findit_interface_text/blob/8.x-1.x/overrides/custom-translations.en.po).

Import with:

```
drush locale:import en web/modules/contrib/findit_interface_text/overrides/custom-translations.en.po --type=customized --override=all
```


## Configuration synchronization is used for individual sites, not for the installation profile

Note that while there is a `config/sync` directory, it is *not* used to install sites as part of development nor will it be used for most active sites (which we expect will be able to account for differences from the distribution with config split or config override).  Instead, the local configuration directory is for the convenience of developers, who can commit where they are and see what has changed, or to send to another developer, towards the end of [packaging all configuration up into features](http://docs.drutopia.org/en/latest/new-content-feature.html ).


### For Cambridge:

Change to the project root, delete the `config` directory if it's already there, and:

```
git clone git@gitlab.com:agaric/sites/find-it-cambridge-config.git config
```

Now you can (from within the virtual machine) `drush -y cim` and `drush -y cex` all you want.


Your local site will live at https://findit-dev.ddev.site/

See [DEVELOPMENT.md](DEVELOPMENT.md) for more, particularly regarding feature development and (for Find It Cambridge) running migrations locally.

See [deploy/REQUIREMENTS.md](deploy/REQUIREMENTS.md) for server requirements.

See [CUSTOMIZATIONS.md](CUSTOMIZATIONS.md) for notes on recommended approaches to necessary and optional modifications for specific instances of the distribution.

See [SEARCH.md](SEARCH.md) for notes on developing the Solr-powered facet filtering and full-text search.

See [DEBUG.md](DEBUG.md) for troubleshooting and debugging recommendations, like how to read logs, especially for live, test, and continuous integration sites.

See [deploy/DEPLOYMENT.md](deploy/DEPLOYMENT.md) for deployment instructions and server information.

#### Drush on live

Temporarily, use:

`/usr/bin/php7.2 ../vendor/bin/drush uli`
