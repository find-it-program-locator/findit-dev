## Customization for Instances of the Find It Program Locator Distribution

Some notes on ways to customize individual sites using the [Find It Program Locator](https://agaric.coop/findit) distribution.


### Address defaults

The addressfield on locations is currently set to default to the United States as the country, Massachusetts as the state/province, and Cambridge as the city.

Using config overrides or whatever system we end up using you'll want to override that file, which if you use `drush -y config:export` will be at:

`config/sync/field.field.node.findit_location.field_findit_address.yml`

Or for it's present location in a built-out development environment:

`web/modules/contrib/drutopia_findit_location/config/install/field.field.node.findit_location.field_findit_address.yml`

If we want to customize this based on service provider for any multi-locality instances of the Find It program locator, this would require custom development.


### Changing text in the interface

Customization for the distribution is done with `.po` files in the [Find It Interface Text Translation Export and Import](https://gitlab.com/find-it-program-locator/findit_interface_text) project, but there's no need for specific instances of the distribution to compete with it nor go through those complications.  Instead, sites can use the override strings option in `settings.php`.

Search for the string you want to change at `/admin/config/regional/translate`.

Add it to the projects `web/sites/default/settings.php` file like this:

```php
$settings['locale_custom_strings_en'][''] = [
  'Add existing @type_singular' => 'Pick existing @type_singular',
];
```

*Note: We're already customizing that one to "Select existing" contact, location, etc.  See Find It Interface Text Translation's [overrides/custom-translations.en.po](https://gitlab.com/find-it-program-locator/findit_interface_text/blob/8.x-1.x/overrides/custom-translations.en.po).*
