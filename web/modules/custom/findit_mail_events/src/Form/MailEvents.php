<?php


namespace Drupal\findit_mail_events\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MailEvents
 */
class MailEvents extends ConfigFormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'findit_mail_events';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $mail_config = $this->config('findit_mail_events.mail');
    $form['mails'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Mails'),
    ];
    $email_token_help = $this->t('Available variables are: [site:name], [site:url], [user:display-name], [user:account-name], [user:mail], [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url], [user:cancel-url].');
    $form['ada_compliance_on_pdf'] = [
      '#type' => 'details',
      '#title' => $this->t('ADA compliance on PDFs mail'),
      '#description' => $this->t('ADA compliance on PDFs.') . ' ' . $email_token_help,
      '#group' => 'mails',
      '#weight' => 10,
    ];
    $form['ada_compliance_on_pdf']['ada_compliance_on_pdf_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('ada_compliance_on_pdf.subject'),
      '#maxlength' => 180,
      '#token_types' => [
        'node',
      ],
      '#element_validate' => ['token_element_validate'],
    ];
    $form['ada_compliance_on_pdf']['ada_compliance_on_pdf_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('ada_compliance_on_pdf.body'),
      '#rows' => 15,
      '#token_types' => [
        'node',
      ],
      '#element_validate' => ['token_element_validate'],
    ];

    $form['outdated_notification'] = [
      '#type' => 'details',
      '#title' => $this->t('Outdated Opportunity mail'),
      '#description' => $this->t('Message sent to the Service Providers for outdated opportunities.'),
      '#group' => 'mails',
      '#weight' => 20,
    ];
    $form['outdated_notification']['outdated_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('outdated_notification.subject'),
      '#maxlength' => 180,
      '#token_types' => [
        'node',
      ],
      '#element_validate' => ['token_element_validate'],
    ];
    $form['outdated_notification']['outdated_notification_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('outdated_notification.body'),
      '#rows' => 15,
      '#token_types' => [
        'node',
      ],
      '#element_validate' => ['token_element_validate'],
    ];

    $form['request_provider_assistance'] = [
      '#type' => 'details',
      '#title' => $this->t('Request provider assistance email default text'),
      '#description' => $this->t('Edit the default e-mail generated for people who choose to request assistance from a service provider.') . ' ' . $this->t('Available variables (tokens) include [node:field_findit_organization:0:entity] for primary organization name, [current-page:url:absolute] for link to the opportunity, and [node:title] for the opportunity name.'),
      '#group' => 'mails',
      '#weight' => 30,
    ];
    $form['request_provider_assistance']['request_provider_assistance_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('request_provider_assistance.subject'),
      '#maxlength' => 180,
    ];
    $form['request_provider_assistance']['request_provider_assistance_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('request_provider_assistance.body'),
      '#rows' => 12,
    ];

    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        'node', 'user',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('findit_mail_events.mail')
      ->set('ada_compliance_on_pdf.subject', $form_state->getValue('ada_compliance_on_pdf_subject'))
      ->set('ada_compliance_on_pdf.body', $form_state->getValue('ada_compliance_on_pdf_body'))
      ->set('outdated_notification.subject', $form_state->getValue('outdated_notification_subject'))
      ->set('outdated_notification.body', $form_state->getValue('outdated_notification_body'))
      ->set('request_provider_assistance.subject', $form_state->getValue('request_provider_assistance_subject'))
      ->set('request_provider_assistance.body', $form_state->getValue('request_provider_assistance_body'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'findit_mail_events.mail',
    ];
  }

}
