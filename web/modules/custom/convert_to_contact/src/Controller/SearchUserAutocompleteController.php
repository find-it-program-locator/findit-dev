<?php

namespace Drupal\convert_to_contact\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class SearchUserAutocompleteController extends ControllerBase {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $userStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);
    $query = $this->userStorage->getQuery();
    $or = $query->orConditionGroup();
    $or->condition('mail', $input, 'CONTAINS');
    $or->condition('name', $input, 'CONTAINS');
    $query->condition($or);
    $query->condition('status', 1)
      ->groupBy('uid')
      ->sort('created', 'DESC')
      ->range(0, 10);

    $ids = $query->execute();
    $users = $ids ? $this->userStorage->loadMultiple($ids) : [];

    /** @var \Drupal\user\UserInterface $user */
    foreach ($users as $user) {
      $label = [
        $user->getDisplayName(),
        "&lt;" . $user->getEmail() . '&gt;',
        '<small>(' . $user->id() . ')</small>',
      ];

      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$user]),
        'label' => implode(' ', $label),
      ];
    }
    return new JsonResponse($results);
  }

}
