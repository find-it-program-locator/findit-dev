<?php

namespace Drupal\convert_to_contact\Form;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConvertUserToContactForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface;
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'convert_user_to_contact_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL) {
    $form['convert_fieldset'] = [
      '#type' => 'fieldset',
    ];
    $form['convert_fieldset']['destiny'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact administrator'),
      '#autocomplete_route_name' => 'convert_to_contact.search_user',
      '#description' => $this->t('The user that will have access to edit this contact data.'),
    ];
    $form['convert_fieldset']['message'] = [
      '#markup' => "<p><strong>Warning: once the account is converted into a contact, the account won't be able to be used to login into the site.</strong></p>",
    ];
    $form['convert_fieldset']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Convert',
    ];
    $form_state->set('user', $user);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['destiny'])) {
      $form_state->setErrorByName('destiny', $this->t('The user that will manage the account contact data is required'));
    }
    $contact_administrator = EntityAutocomplete::extractEntityIdFromAutocompleteInput($values['destiny']);

    if (empty($contact_administrator)) {
      $form_state->setErrorByName('destiny', $this->t('The user cannot be found.'));
    }
    $user = $form_state->get('user');
    if ($contact_administrator == $user->id()) {
      $form_state->setErrorByName('destiny', $this->t('The user cannot manage their own contact data.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, $user = NULL) {
    $user = $form_state->get('user');
    $values = $form_state->getValues();
    $contact_administrator = EntityAutocomplete::extractEntityIdFromAutocompleteInput($values['destiny']);

    // User contacts.
    $query = $this->entityTypeManager->getStorage('profile')->getQuery();
    $query->condition('crm_created_by', $user->id());
    $profiles = $query->execute();

    // User own profiles.
    $query = $this->entityTypeManager->getStorage('profile')->getQuery();
    $query->condition('uid', $user->id());
    $user_profiles = $query->execute();
    $profiles = $profiles + $user_profiles;

    // Assing all the profiles to the new administrator.
    foreach ($profiles as $profile_id) {
      $profile = $this->entityTypeManager->getStorage('profile')->load($profile_id);
      $profile->set('crm_created_by', $contact_administrator);
      $profile->save();
    }
    // Block the user.
    $user->set('status', 0);
    $user->save();

    // Redirect to the user page.
    $this->messenger()->addMessage($this->t('The user has been converted successfully and it is now managed by @mail,', ['@mail' => $values['destiny']]));
    $form_state->setRedirect('entity.user.edit_form', ['user' => $user->id()]);
  }

}
