<?php

namespace Drupal\findit_upgrade\Plugin\migrate\destination;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\DependencyTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Contact
 *
 * @package Drupal\findit_upgrade\migrate\destination
 *
 *  @MigrateDestination(
 *   id = "findit_contact",
 * )
 */
class Contact extends DestinationBase implements ContainerFactoryPluginInterface, DependentPluginInterface {

  use DependencyTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface;
   */
  protected $entityTypeManager;

  /**
   * Constructs an entity destination plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->migration = $migration;
    $this->entityTypeManager = $entityTypeManager;
    $this->supportsRollback = TRUE;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $this->addDependency('module', 'profile');
    return $this->dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'name' => 'The name of the contact.',
      'status' => 'The status of the contact',
      'mail' => 'The mail associated with the profile.',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $original_name = $row->getDestinationProperty('name');
    $status = $row->getDestinationProperty('status');
    $mail = !empty($row->getDestinationProperty('mail')) ? $row->getDestinationProperty('mail')[0]['email'] : '';
    $phone = $row->getDestinationProperty('crm_phone');

    // One word names are going to be migrated as organizations.
    $name = explode(" ", $original_name);
    if (count($name) == 1 || count($name) >= 3) {
      $name_field = 'crm_org_name';
      $role = 'crm_org';
      $type = 'crm_org';
      $name = [0 => ['value' => $original_name]];
    }
    else {
      $name_field = 'crm_name';
      $role = 'crm_indiv';
      $type = 'crm_indiv';
      $given_name = $name[0];
      unset($name[0]);
      $family_name = implode(" ", $name);
      $name = [0 => ['given' => $given_name, 'family' => $family_name]];
    }

    // Before to create the user let's check if there is already one with the
    // same email.
    $user_query = \Drupal::entityQuery('user');
    $user_query->condition('mail', $mail);
    $ids = $user_query->execute();
    $id = array_pop(array_reverse($ids));

    if (!empty($id)) {
      $user = User::load($id);
    }
    else {
      // Create the user associated with the profile.
      /** @var \Drupal\user\Entity\User $user */
      $user = $this->entityTypeManager->getStorage('user')->create();
      $user->setValidationRequired(FALSE);
      $user->set('mail', $mail);
      // Add our relevant role.
      $user->addRole($role);
      $user->save();

    }

    // If the profile was already created by the user migration we just need
    // update the values.
    $profile = $this->entityTypeManager->getStorage('profile')->loadByProperties(['type' => $type, 'uid' => $user->id()]);
    $profile = reset($profile);
    if ($profile === FALSE) {
      /* @var \Drupal\profile\Entity\ProfileInterface $profile */
      $profile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => $type,
        'status' => $status,
        'is_default' => TRUE,
      ]);
    }

    $profile->setValidationRequired(FALSE);
    $profile->set($name_field, $name);
    $profile->set('crm_phone', $phone);
    $profile->setOwner($user);

    $profile->set('field_findit_crm_role', $row->getDestinationProperty('field_findit_crm_role'));
    $profile->set('field_findit_crm_ext', $row->getDestinationProperty('field_findit_crm_ext'));
    $profile->set('field_findit_crm_tty_number', $row->getDestinationProperty('field_findit_crm_tty_number'));
    $profile->set('field_public_email', $mail);
    $profile->set('crm_created_by', $row->getDestinationProperty('crm_created_by'));
    $profile->save();

    return ['id' => $profile->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $id = $destination_identifier['id'];
    /* @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $this->entityTypeManager->getStorage('profile')->load($id);
    if ($profile !== NULL) {
      $profile->delete();
    }
  }
}
