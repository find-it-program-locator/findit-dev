<?php

namespace Drupal\findit_upgrade\Plugin\migrate\destination;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Row;
use Drupal\user\Plugin\migrate\destination\EntityUser;
use Drupal\user\Entity\User as UserEntity;


/**
 * Class Contact
 *
 * @package Drupal\findit_upgrade\migrate\destination
 *
 *  @MigrateDestination(
 *   id = "findit_user",
 * )
 */
class User extends EntityUser  implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $user_id = parent::import($row, $old_destination_id_values);

    $id = $user_id;
    $user = UserEntity::load(array_pop($id));

    // Create the profile to store the name, organization and phone.
    /* @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = \Drupal::entityTypeManager()->getStorage('profile')->create([
      'type' => 'crm_indiv',
      'status' => 1,
      'is_default' => TRUE,
    ]);

    $given_name = $row->getDestinationProperty('field_first_name');
    $given_name = !empty($given_name[0]['value']) ? $given_name[0]['value'] : NULL;

    $family_name = $row->getDestinationProperty('field_last_name');
    $family_name = !empty($family_name[0]['value']) ? $family_name[0]['value'] : NULL;

    $phone = $row->getDestinationProperty('field_phone_number');
    $phone = !empty($phone[0]['value']) ? $phone[0]['value'] : NULL;

    $name = [0 => ['given' => $given_name, 'family' => $family_name]];
    $profile->setValidationRequired(FALSE);
    $profile->set('crm_name', $name);
    $profile->set('crm_phone', $phone);
    $profile->set('field_public_email', $row->getDestinationProperty('mail'));
    $profile->setOwner($user);
    $profile->set('field_findit_crm_role', 'crm_indiv');
    $profile->save();

    return $user_id;
  }

}
