<?php

namespace Drupal\findit_upgrade\Plugin\migrate\process;

use Drupal\migrate_plus\Plugin\migrate\process\SkipOnValue;

/**
 * Class InArray
 * @MigrateProcessPlugin(
 *   id = "skip_on_value_array"
 * )
 */
class InArray extends SkipOnValue {

  /**
   * Compare values to see if the value is in the array.
   *
   * @param mixed $value
   *   Actual value.
   * @param array $skipValue
   *   Value to compare against.
   * @param bool $equal
   *   Compare as equal or not equal.
   *
   * @return bool
   *   True if the compare successfully, FALSE otherwise.
   */
  protected function compareValue($value, $skipValue, $equal = TRUE) {
    if ($equal) {
      return in_array($skipValue, $value);
    }
    return !in_array($skipValue, $value);
  }

}
