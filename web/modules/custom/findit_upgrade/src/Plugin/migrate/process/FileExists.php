<?php


namespace Drupal\findit_upgrade\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "findit_file_exists"
 * )
 */
class FileExists extends ProcessPluginBase {


  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;


  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($source, $destination) = $value;

    // Ensure the source file exists.
    if (!file_exists($source)) {
      throw new MigrateSkipRowException("File '$source' does not exist");
    }
    return $value;
  }
}
