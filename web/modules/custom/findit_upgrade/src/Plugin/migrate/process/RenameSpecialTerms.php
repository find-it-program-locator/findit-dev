<?php

namespace Drupal\findit_upgrade\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "find_it_rename_special_terms"
 * )
 */
class RenameSpecialTerms extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_value = $value;
    if ($value == 'Basic Needs (Clothing, Diapers, Fuel Assistance, etc…)') {
      $new_value = 'Utilities';
    }

    if ($value == 'Early Childhood Activities (Playgroups, Story Time, Sing-a-Long, etc...)') {
      $new_value = 'Young Children & Parent Activities';
    }

    return $new_value;
  }
}
