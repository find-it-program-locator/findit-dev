<?php

namespace Drupal\findit_upgrade\Plugin\migrate\source;

use Drupal\path\Plugin\migrate\source\d7\UrlAlias;

/**
 * Drupal 7 Alias migration.
 *
 * @MigrateSource(
 *   id = "find_it_urlalias",
 *   source_module = "path"
 * )
 */
class FinditUrlAlias extends UrlAlias {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // The contact content type wasn't migrated, no need to migrate its alias
    // and skip all the "search" aliases.
    $select = parent::query();
    $select->condition('alias', $this->database->escapeLike('contact') . "%", 'NOT LIKE');
    $select->condition('source', "%" . $this->database->escapeLike('search') . "%", 'NOT LIKE');
    return $select;
  }

}
