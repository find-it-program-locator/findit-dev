<?php

namespace Drupal\findit_upgrade\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * Drupal 7 age migration.
 *
 * It migrate the field age.
 *
 * @MigrateSource(
 *   id = "findit_age",
 *   source_module = "findit_upgrade"
 * )
 */
class Age extends SqlBase {

  /**
   * {@inheritdoc}
   * The configuration of a field is serialized in the database so we cannot
   * use directly the SqlBase::initializeIterator(), so let's write our own.
   */
  protected function initializeIterator() {
    $values = $this->getFieldValues();
    $object = new \ArrayObject($values);
    $iterator = $object->getIterator();

    return new \IteratorIterator($iterator);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('id'),
      'name' => $this->t('Name'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    $ids['id']['alias'] = 'fc';

    return $ids;
  }

  public function count($refresh = FALSE) {
    $values = $this->getFieldValues();
    return count($values);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $select = $this->select('field_config', 'fc')
      ->fields('fc', [
        'data',
      ])
      ->condition('field_name', 'field_age_eligibility');

    return $select;
  }

  /**
   * Unserialize and return the allowed values of the field.
   *
   * @return array
   */
  protected function getFieldValues() {
    $statement = $this->query()->execute();
    $statement->setFetchMode(\PDO::FETCH_ASSOC);
    $data = $statement->fetchAssoc();
    $data = unserialize($data['data']);
    $values = [];
    foreach($data['settings']['allowed_values'] as $key => $value) {
      $values[] = ['id' => $key, 'name' => $value];
    }

    return $values;
  }

}
