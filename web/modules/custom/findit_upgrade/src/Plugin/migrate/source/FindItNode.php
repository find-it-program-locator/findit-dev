<?php

namespace Drupal\findit_upgrade\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Drupal 7 node source from database (skip the soft deleted nodes).
 *
 * @MigrateSource(
 *   id = "find_it_node",
 *   source_module = "node"
 * )
 */
class FindItNode extends Node {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    $query->leftJoin('killfile_nodes', 'kf', 'kf.nid = n.nid');
    $query->isNull('kf.timestamp');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $return = parent::prepareRow($row);
    // The "All grades" term is going to be removed so if the entity has that
    // term then mark all the other grades.
    $nid = $row->getSourceProperty('nid');
    $vid = $row->getSourceProperty('vid');
    $field_name = 'field_grade_eligibility';

    $field_grade_eligibility = $this->getFieldValues('node', $field_name, $nid, $vid);
    // Check if the "All grade" term has been selected.
    if (empty($field_grade_eligibility)) {
      return $return;
    }

    foreach ($field_grade_eligibility as $index => $value) {
      if ($value['tid'] != 5) {
        continue;
      }
      $value = $this->getAllGrades();
      $row->setSourceProperty($field_name, $value);
    }
    return $return;
  }

  /**
   * Return all the grades.
   */
  protected function getAllGrades() {
    $query = $this->select('taxonomy_term_data',  'ttd');
    $query->fields('ttd', ['tid']);
    $query->condition('vid', 3);
    $query->condition('tid',5, '!=');
    $result = $query->execute();

    $tids = [];
    foreach ($result as $item) {
      $tids[] = $item;
    }
    return $tids;
  }
}
