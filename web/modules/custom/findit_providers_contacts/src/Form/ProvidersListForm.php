<?php

namespace Drupal\findit_providers_contacts\Form;

use Drupal\Core\Config\StorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drutopia_findit_organization\FinditOpportunityNode;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProvidersListForm extends FormBase {

  /**
   * An instance of the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function getFormId(): string {
    return 'form_api_example_simple_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Filter User by content'),
    ];

    $form['activities'] = [
      '#type' => 'select',
      '#title' => $this->t('Activities'),
      '#options' => $this->getTermsOptions('findit_activities'),
    ];

    $form['services'] = [
      '#type' => 'select',
      '#title' => $this->t('Services'),
      '#options' => $this->getTermsOptions('findit_services'),
    ];

    $form['ages'] = [
      '#type' => 'select',
      '#title' => $this->t('Ages'),
      '#options' => $this->getTermsOptions('findit_ages'),
    ];

    $form['dates_position'] = [
      '#title' => $this->t('Select date of the event '),
      '#type' => 'radios',
      '#options' => [
        'past' => $this->t('Past'),
        'future' => $this->t('In the future'),
        'not_include' => $this->t('Do not use date filter'),
      ],
      '#default_value' => 'not_include',
      '#description' => $this->t('Used to filter content that has a date field.'),
    ];

    $form['user_type'] = [
      '#type' => 'radios',
      '#options' => [
        'provider' => $this->t('Provider'),
        'contact' => $this->t('Contact'),
        'provider_and_contact' => $this->t('Both'),
      ],
      '#title' => $this->t('User type'),
      '#default_value' => 'provider',
    ];

    $form['content_type'] = [
      '#title' => $this->t('Select content type'),
      '#type' => 'checkboxes',
      '#options' => [
        'findit_program' => $this->t('Program'),
        'findit_organization' => $this->t('Organization'),
        'findit_place' => $this->t('Place'),
        'findit_event' => $this->t('Event'),
      ],
      '#description' => $this->t('If none is selected, all content types will be used'),
    ];

    $form['published'] = [
      '#type' => 'radios',
      '#options' => [
        '1' => $this->t('Published'),
        '0' => $this->t('Not Published'),
        'all_content' => $this->t('Both'),
      ],
      '#title' => $this->t('Content status'),
      '#default_value' => 1,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    $form['#attached']['library'][] = 'views_data_export/views_data_export';

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->setRebuild(TRUE);
    $values = $form_state->getValues();
    $activities = $values['activities'];
    $services = $values['services'];
    $ages = $values['ages'];
    $user_type = $values['user_type'];
    $published = $values['published'];
    $content_types = $values['content_type'];
    $dates_position = $values['dates_position'];

    $nodes_ids = $this->getData($activities, $services, $ages, $published, $content_types);

    if (empty($nodes_ids)) {
      $this->messenger()
        ->addWarning($this->t('Content not found by given filters.'));
      return;
    }
    $operations = [];
    $chunks = array_chunk($nodes_ids, 10);

    foreach ($chunks as $chunk) {
      $operations[] = [
        static::class . '::batchGetUser',
        [$chunk, $user_type, $dates_position],
      ];
    }

    $batch = [
      'operations' => $operations,
      'finished' => static::class . '::batchGetUserFinished',
      'title' => $this->t('Processing Data'),
      'init_message' => $this->t('Batch starting.'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Batch has encountered an error.'),

    ];
    batch_set($batch);
  }

  /**
   * Get content node by given filter parameter.
   *
   * @param string $activities
   * @param string $services
   * @param string $ages
   * @param string $published
   * @param array $content_types
   *
   * @return array|int
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getData(string $activities, string $services, string $ages, string $published, array $content_types): array|int {

    $query = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->latestRevision();
    if ($published != 'all_content') {
      $query->condition('status', $published);
    }

    $group = $query->orConditionGroup();
    $at_least_one_type_selected = FALSE;
    foreach ($content_types as $key => $content_type) {
      if ($key == $content_type) {
        $group->condition('type', $content_type);
        $at_least_one_type_selected = TRUE;
      }
    }
    if ($at_least_one_type_selected) {
      $query->condition($group);
    }

    if ($activities != 0) {
      $query->condition('field_findit_activities', $activities);
    }
    if ($services != 0) {
      $query->condition('field_findit_services', $services);
    }
    if ($ages != 0) {
      $query->condition('field_findit_ages', $ages);
    }
    return $query->execute();
  }

  /**
   * Get term in the given taxonomy.
   *
   * @param string $vid
   *
   * @return string[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getTermsOptions(string $vid): array {
    $terms_options = [0 => 'Select'];
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadTree($vid);
    foreach ($terms as $term) {
      $terms_options[$term->tid] = $term->name;
    }
    return $terms_options;
  }

  /**
   * Get content date field occurrence if it was in the past or will be in the
   * feature. Applied only for content field with
   * 'field_findit_opportunity_dates' field.
   *
   * @param $node
   *
   * @return array
   */
  public static function getNodeDateData($node): array {
    $return = [
      'past' => FALSE,
      'future' => FALSE,
    ];
    $date_field = 'field_findit_opportunity_dates';
    if (!$node->hasField($date_field)) {
      return $return;
    }
    $raw_dates = $node->get($date_field)->getValue();
    $now = time();
    foreach ($raw_dates as $item) {
      if ($item['end_value'] < $now) {
        $return['past'] = TRUE;
      }
      if ($item['end_value'] > $now) {
        $return['future'] = TRUE;
      }
    }
    return $return;
  }

  /**
   * @param array $operation_details
   * @param string $user_type
   * @param string $dates_position
   * @param array $context
   *
   * @return void
   */
  public static function batchGetUser(array $operation_details, string $user_type, string $dates_position, array &$context): void {
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_node'] = 0;
      $context['sandbox']['max'] = 10;
    }

    foreach ($operation_details as $nid) {
      $node = Node::load($nid);
      if ($dates_position != 'not_include') {
        $dates = self::getNodeDateData($node);
        if (!$dates[$dates_position]) {
          continue;
        }
      }

      if ($user_type === 'provider' || $user_type === 'provider_and_contact') {
        $uid = $node->getOwnerId();
        $user = User::load($uid);
        $user_email = $user->getEmail();
        $user_name = $user->getAccountName();
        $provider_key = array_search($user_email, array_column($context['results'], 'email'));
        if ($provider_key === FALSE) {
          $context['results'][] = [
            'name' => $user_name,
            'email' => $user_email,
            'type' => 'author',
          ];
        }
      }

      if ($user_type === 'contact' || $user_type === 'provider_and_contact') {
        if (!$node->hasField('field_findit_contacts')) {
          continue;
        }
        $contacts = $node->get('field_findit_contacts')->referencedEntities();
        if (!empty($contacts)) {
          foreach ($contacts as $contact) {
            $email = $contact->get('field_public_email')->getValue();
            if (empty($email)) {
              continue;
            }
            $name = $contact->get('crm_org_name')->getValue();
            $contact_key = array_search($email[0]['value'], array_column($context['results'], 'email'));
            if ($contact_key === FALSE) {
              $context['results'][] = [
                'name' => $name[0]['value'],
                'email' => $email[0]['value'],
                'type' => 'contact',
              ];
            }
          }
        }
      }

      $context['sandbox']['progress']++;
      $context['sandbox']['current_node'] = $nid;
      $context['message'] = t('Processing node "@id"',
        ['@id' => $nid]
      );
    }

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = ($context['sandbox']['progress'] <= $context['sandbox']['max']);
    }
  }

  /**
   * @param bool $success
   * @param array $results
   * @param array $operations
   *
   * @return void
   */
  public static function batchGetUserFinished(bool $success, array $results, array $operations): void {
    $messenger = \Drupal::messenger();

    if ($success) {
      if (!empty($results)) {
        $directory = "public://provider_export/";
        $timestamp = \Drupal::time()->getRequestTime();
        $date = \Drupal::service('date.formatter')
          ->format($timestamp, 'custom', 'c');
        $filename = sprintf('providers_export_%s.csv', $date);

        $fileSystem = \Drupal::service('file_system');
        $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
        $destination = $directory . $filename;
        $file = \Drupal::service('file.repository')
          ->writeData('', $destination, FileSystemInterface::EXISTS_REPLACE);
        if (!$file) {
          throw new StorageException('Could not create a temporary file.');
        }
        $file->setTemporary();
        $file->save();

        $pe_file = $file->getFileUri();

        $handle = fopen('php://temp', 'w+');
        $header = [
          'Name',
          'Email',
          'Type',
        ];
        fputcsv($handle, $header);
        foreach ($results as $result) {
          $data = [
            $result['name'],
            $result['email'],
            $result['type'],
          ];
          fputcsv($handle, array_values($data));
        }
        rewind($handle);
        $csv_data = stream_get_contents($handle);
        fclose($handle);

        file_put_contents($pe_file, $csv_data, FILE_APPEND) === FALSE;

        $url = \Drupal::service('file_url_generator')
          ->generateAbsoluteString($pe_file);
        $message = t('Export completed. Click <a download href=":download_url"  data-download-enabled="true" id="vde-automatic-download">here</a>  if file is not automatically downloaded.', [':download_url' => $url]);
        \Drupal::messenger()->addMessage($message);
      }
      else {
        $messenger->addMessage(
          t('Emails not found by given filters.')
        );
      }
    }
    else {
      $messenger->addMessage(
        t('Export failed. Make sure the file system is configured and check the error log.')
      );
    }
  }
}
