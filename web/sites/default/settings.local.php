<?php
// Local settings as used on live/test or modified for the dev environment.

$config['system.site']['name'] = 'Find It Cambridge';

// Override configuration to make it work solr on local (ddev)
$config['search_api.server.solr_development_'] = [
  'backend_config' => [
    'connector_config' => [
      'host' => 'solr',
      'path' => '/',
      'core' => 'drupal',
      'port' => '8983',
      'advanced' => ['solr_install_dir' => '/opt/solr'],
    ],
  ],
];


// Override configuration to use `php_mail` on local (with ddev).
// this allows mailHog to catch the mails and prevent to sent them via SMTP.
$config['mailsystem.settings']['defaults']['sender'] = 'php_mail';
$config['mailsystem.settings']['modules']['sendgrid_integration']['none']['sender'] = 'php_mail';

// The above works on local where DDEV captures PHP but for the test site we do this:
/*
$config['mailsystem.settings']['defaults']['sender'] = 'test_mail_collector';
$config['mailsystem.settings']['modules']['sendgrid_integration']['none']['sender'] = 'test_mail_collector';
$config['system.mail']['interface']['default'] = 'test_mail_collector';
*/

$config['config_split.config_split.local']['status'] = TRUE;
