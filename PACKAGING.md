# OBSOLETE Packaging

Once the distribution has been built for findit-dev here, we can spare other sites on the platform the overhead of running composer install.

In this example, we have created a `find-it-cambridge` directory alongside our `findit-dev` directory (or checked out an existing site-specific repo to the same place).

```
./package ../find-it-cambridge/
```

The failed patches for facets module still cause us problems though, so the packaging script removes local modifications from the packaged directory (facets.module.rej and PATCHES.txt in web/modules/contrib/facets/)

From inside the `find-it-cambridge` directory/repository, the following will complete the cleanup:

```
git add web/modules/contrib/facets/
```

Note the trailing slash, that's important.

Everything else can be committed with `git add .` without a second thought.
