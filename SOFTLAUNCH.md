* DONE: Package up Find It Cambridge site from distribution -ben
* Add deployment scripts to https://gitlab.com/find-it-program-locator/find-it-cambridge -chris
* DONE: Enable Contacts to save without e-mail addresses -ben
* Fix Entity Reference suggestion filtering to actually work for contacts - David
* Improve general first impression styling -ben
* Test what happens when referencing unpublished content, and either prevent publishing until the referenced content is also published, or (easier) disallow referencing unpublished content.
* Remove Event permissions from Service Providers in site-specific config -ben
* Disable user registration in site-specific config -ben
* Configure menu on site -ben

NEW discovered-while-offline TODOs
* Chosen is doing really stupid things with its auto-calculated width.  We're going to need to do a lot with this multiselect functionality so look quickly for alternatives (that maybe aren't such bad maintainers in terms of welcoming outside contributions, and that maybe haven't thrown up their hands at the thought of mobile).
