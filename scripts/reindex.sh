#!/bin/bash
set -e
# Clear index fully.
drush sapi-sc solr_development_
drush sapi-c
# Now, re-index.
drush sapi-i
