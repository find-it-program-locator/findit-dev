#!/bin/bash
set -euxo pipefail
if [ "$1" == "live" ]
then
        webroot="/var/www/fic"
        php=""
elif [ "$1" == "test" ]
then
        webroot="/var/www/fic_test"
        php=""
elif [ "$1" == "dev" ]
then
	webroot="/var/www/html"
	drush="drush"
  php=""
else
        echo "$0 ERROR: Supply one of: test, live, dev"
        exit 1
fi
drush="${php} ${webroot}/vendor/bin/drush -r ${webroot}/web"

${drush} cr
${drush} updb -y
${drush} cim -y
${drush} smart_date:migrate findit_program field_findit_opportunity_dates field_findit_dates
${drush} smart_date:migrate findit_event field_findit_opportunity_dates field_findit_dates
${drush} smart_date:migrate findit_program field_findit_register_dates field_findit_registration_dates
${drush} smart_date:migrate findit_event field_findit_register_dates field_findit_registration_dates
${drush} cr
