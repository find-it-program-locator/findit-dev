# Find It Platform Development

To work with features, it is first necessary to enable Features UI; this module does that and a little bit more:

```
drush -y en drutopia_dev_findit
```

## Features-based Development

We are using the `drutopia_findit` namespace.

Note that as far as Features itself is concerned, we are simply using the `drutopia` namespace.  Features will automatically prefix the Feature modules it creates with `drutopia_` so this means our machine name should begin `findit_`.

Remember to work with Features only with "Drutopia" selected as the Bundle on the [Features development page](https://findit-dev.ddev.site/admin/config/development/features ).

Caution:  If you press the Back button to return to that Features listing page, and possibly in other situations also, the drop-down select will still *say* Drutopia but actually be the Default that we don't want, which is evidenced by it only having a handful of features.

Features doesn't always understand automatically which fields should go with which Feature, so check carefully.  Because we are using this non-standard extra `findit_` namespace, this isn't surprising, but oddly it will often assign the bundle-level instance of a field correctly to the Feature you are working on while assigning the field storage of the same field incorrectly to the Find It profile (`findit`), which we never want.

To recapture Features' incorrectly-assigned fields, checkmark "Allow conflicts" in the left-hand column.  Then it is useful to use the Search feature at the top of the right-hand **Components** column to more quickly and easily find the fields you just created.

Double-check that the **Bundle** is Drutopia, that the **Version** is `8.x-1.0-alpha1` (following [Drutopia's convention for pre-release features](http://docs.drutopia.org/en/latest/new-content-feature.html#creating-feature-and-determining-where-fields-go)).

Make sure the **Path** is `modules/contrib`.

Then press the Write button.  The first time you do this, you'll need to change directory to the created directory and `git init` and create a project within the [Find It GitLab organization] with the same name as the written module (e.g. `drutopia_findit_whatever`); follow the rest of the instructions GitLab provides for your initial commit.

Henceforth you can `cd` to the directory and `git add` your changes, review your changes with `git diff --cached`, and then `git commit` your changes and `git push` them up to the project.

Also when adding a new Feature module add it to the dependencies of [the Find It profile](https://gitlab.com/find-it-program-locator/findit)'s `findit.info.yml` file.

As noted, we don't add any fields to the profile, because we want the Drutopia Find It features to be usable outside the profile.  We're not aiming for full independence of the feature modules, though.  In general, if a field is used by multiple feature modules the first module on this list, in this order, should be where the storage part lives: `drutopia_findit_organization`, `drutopia_findit_program`, `drutopia_findit_event`, `drutopia_findit_place`.  A module using another module's field should therefore include the module whose fields they use as a dependency.


## Needs for the distribution/platform

Because the initial instance is migrating in taxonomy terms, we aren't creating default taxonomy terms.  For the distribution/platform, we will want to create default terms for:

* Ages
* Accessibility
* Amenities
* Day of Week
* Eligibility
* Grades
* Neighborhoods
* Services
* Special times
* Time of day

Of these, we can use Cambridge's without change for Ages, Accessibility, Amenities, Day of week, Grades, School breaks, Services, Special times, Time of day.

Eligibility just needs to be genericized a little bit.

Neighborhoods we could create a placeholder in-city term and, or maybe just only, the out-of-city term.

I'd like to add ASL to Accessibility, as sign language interpretation being available appeared in notes several times in Cambridge, but of course we can add that to Cambridge too.


## Running the migration in development

Download a database dump of the D7 site.

Create a database called `drupal7`:

```
sudo su
mysql --host=127.0.0.1 --user=root
CREATE DATABASE drupal7;
GRANT ALL ON drupal7.* to 'db'@'%' IDENTIFIED BY 'db';
```

Now `ctrl+c` out of there, and, import your file from whereever you placed and named your db SQL dump:

```
mysql --host=127.0.0.1 --user=drupal --password=drupal --database=drupal7 < data/db.mysql
```

Then add this drupal7 database to your local.settings.php file

```
$databases['drupal7']['default'] = array(
  'database' => 'drupal7',
  'username' => 'drupal',
  'password' => 'drupal',
  'host' => $host,
  'driver' => 'mysql',
  'port' => $port,
  'prefix' => '',
);
```

Next enable the `findit_upgrade` module.
 
To see the available migrations, run 
```
drush ms 
```

To run the drupal7 migration run 

`drush mim --group="migrate_drupal_7"`


## Miscellaneous Temporary Notes

* Run queres on `ssh findit-vm`:
```
SELECT * FROM field_data_field_contact_phone_extension: 53 rows.  One person put the whole phone number; one person put "x28", and all the rest are numbers except one person who put "Option 0"...  should we force this to be an integer?
SELECT * FROM field_data_field_contact_tty_number - 19 rows.  One person
put a redundant TTY.
SELECT * FROM field_data_field_tumblr_url; - 21 rows so we'll migrate it to a generic Blog field.  Also, all of them are City of Cambridge - Community Development Dept. which brings up a question: Do we want to try to *discourage* events and programs from listing the *same* social media information as an associated organization?  Either by making it easy to expose that information on the event/program page or just by hiding it?  TODO add to meeting agenda.
```


* Ask Clayton to post issue about deciding whether to use plain text fields for phone numbers or a D8 phone field module.  Would be good to check to see why Contacts/DecoupledAuth made the call to go with plain text fields (i'm doing the same now as i'm offline and can't install another module anyway).  Does any phone module have an extension field?  That would be nice to have a compound field.
* Ask Clayton to post asking Contacts if there's a way to convert from organization to individual and vice versa built in perhaps?
* Ask Clayton to post issue to change, with JavaScript, "Registration" => "Application".
* Ask Clayton to add a user story: "A site manager can see which organizations, programs, etc. reference a given contact."  And "A site manager can see which organizations, programs, etc. reference a given location."

* Need to record the development needed to hide the 'free' option on   `field_findit_cost_subsidies` (but auto-select/save it when `field_findit_gratis_noindex` is TRUE.


It would be really nice to be able to make some fields conditionally required based on what other fields are set to, but it appears that is currently broken: [Conditional Required Fields not Evaluating](https://www.drupal.org/project/conditional_fields/issues/2859667 )

For example, for some settings of `field_findit_cost_subsibies` (possibly new ones to be added) it should be required to have the financial aid notes, file, or URL fields provided.


---

When we need to allow site managers to add arbitrary information that
needs to be stored somewhere, instead of rolling our own config pages we
can use https://www.drupal.org/project/site_settings or/and https://www.drupal.org/project/config_pages


---

If you see an error like this when running `composer fresh-start`:

```
> drush -y si findit && drush -y en drutopia_dev_findit && drush uli
PHP Fatal error:  Uncaught TypeError: Return value of Symfony\Component\Filesystem\Filesystem::toIterable() must be an instance of Symfony\Component\Filesystem\iterable, array returned in /home/mlncn/Projects/find-it-program-locator/findit-dev/vendor/symfony/filesystem/Filesystem.php:732
```

calm down, don't panic, do not investigate it at all....  just run:

```
ddev composer fresh-start
```
