# Deployment Guide

## Server-side utilities:

A few scripts and shell aliases are available on the server for performing regular maintenance activities there. Scripts that are not instance-specific per their naming can accept an argument of `live` or `test` for the target desired.

* `backup_db.sh` - creates a dated backup of the specified database.
* `copy_new_to_test.sh` - backup the test db (to paranoia in backups) and copy database and files from live to test. Uses ionice, etc to reduce performance impact.
* `cronjob.sh` - run by cron.
* `live-log.sh` - display the log from live for the last 5 minutes.
* `reindex.sh` - fully-recreate the Solr index for the specified instance.
* `drush_live` & `drush_test` - aliases that will automatically target the live / test instance.

## Server monitoring:

Drupal logs are viewable on the system with `journalctl -t drupal`. Review usage of journalctl for more information.

Installation logs are recorded (for the code installation - does not include all drush command output) automatically at ~agaric/installs/logs/installer.log

## Local environment configuration for deployments:

It is recommended to use [direnv](https://direnv.net/) or similar tool to provide automatic environment configuration. Otherwise, you may add the variables defined for `.envrc` below to your shell configuration (e.g. `~/.bash_profile`) or else source the `.envrc` every time you would like to deploy by running: `. .envrc` in the active shell. Either way, you must configure the environment variables.

To set up `direnv` (only required once):

1. Configure a `<find it project>/.envrc` file with the following. You can also `cp .envrc.example .envrc` and edit from there:

```
# Path to build folder for Drutopia (normally {host project}/build)
export DRUTOPIA_BUILD_ROOT="~/Projects/drutopia-platform/drutopia_host/build"
# Path to config project
export FIC_CONFIG_PROJ="~/Projects/find-it-cambridge-config"
```
2. Tell direnv to trust the newly created .envrc file: `direnv allow`

direnv will now automatically load your local `.envrc` when you enter the FindIt project folder.

## Building the codebase:

This procedure defines the current deployment routine.

1. Build the code using the Drutopia hosting package:

Note the first command requires [gitlab.com/drutopia-platform/hosting_private](https://gitlab.com/drutopia-platform/hosting_private/) be located per setup in its README.

```bash
pushd $DRUTOPIA_BUILD_ROOT/../hosting_private
ahoy git-pull-all
ahoy deploy-build findit-ci-new
popd
```

2. Ensure the local find-it-cambridge-config repository is up to date:

```bash
cd ~/Projects/find-it-program-locator/findit-dev
deploy/pull-config.sh
```

3. Determine the next-highest numbered to use for the install. See the existing folders with e.g. `ssh findit-vm "ls -al installs"`

4. Upload the files to the server: `./deploy/push-release.sh XX` where XX is the new folder number to create under `agaric@findit-vm:installs/`

For reference, the following actions are performed when running `push_release.sh XX`:

```
mkdir findit-vm:installs/XX
rm sync.tgz
tar -c -z -f sync.tgz -C ${FIC_CONFIG_PROJ} sync
scp $DRUTOPIA_BUILD_ROOT/findit-ci-new.tgz findit-vm:~/installs/XX/
scp $DRUTOPIA_BUILD_ROOT/VERSION  findit-vm:/home/agaric/installs/XX/
scp $DRUTOPIA_BUILD_ROOT/install.sh findit-vm:~/installs/XX/
scp scripts/update.sh findit-vm:~/installs/XX/
scp sync.tgz findit-vm:~/installs/XX/
```

1. Continue from the server - `ssh findit-vm`
1. On server, perform backups as needed (`backup_db.sh test`), or use `copy_new_to_test.sh` to refresh the test instance (which also backs up existing test db).
1. The scripts expect to find current code in the current working directory, so it is imperative to first change to the proper install folder: `cd installs/XX/`. Then you are ready to begin. Copy configuration into place `config_copy.sh test XX`. The first argument is test or live, the second argument is the name of a folder to move the existing config into - typically the install number. e.g. `config_copy.sh live 42` will copy `/var/www/fic/config/sync` into `/var/www/fic/config/_attic/pre_42/`
1. Now install the new codebase: `install_new_code.sh test` (arguments are either: test or live). Note that the installer is run in the background, and the install script then tail's the install log. When it says "Successfully installed..." (or else errors), you can CTRL+C to continue.
1. Update the settings.local.php per each site, if needed. These are currently managed manually.
1. Run upgrade (updb and config import). Because some upgrades may require special steps, an update.sh is provided in the current source and is also uploaded to the install folder. Run `./update.sh live` or `./update.sh test` as appropriate.
