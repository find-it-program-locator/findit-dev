# Requirements

## A large maximum file upload capacity

Used `sudo su` root access to change `/etc/php/7.2/fpm/php.ini`:

```
post_max_size = 36M
upload_max_filesize = 32M
```

And then `/etc/init.d/php7.2-fpm reload`

On Cambridge's virtual servers, we expect this setting will stay preserved short of a major OS update.
