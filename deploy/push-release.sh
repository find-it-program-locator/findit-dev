#!/bin/bash
#set -euxo pipefail
set -euo pipefail

# These should be in .envrc:
# Path to build folder for Drutopia (normally {host project}/build)
# DRUTOPIA_BUILD_ROOT="/home/wolcen/Agaric/Drutopia/host/build"
# Path to config project
# FIC_CONFIG_PROJ="/home/wolcen/Agaric/find-it-cambridge-config"

function usage() {
  echo "Usage: $0 release_no"
  echo
  echo "  release_no integer representing a release number"
  echo "             See:  ssh findit-vm \"ls -al installs\""
  echo
}
# Check for arg1
[ $# -ne 1 ] && (usage; exit 1)
# Check folder locations
if [ -z "${DRUTOPIA_BUILD_ROOT+x}" ] \
  || [ ! -d "${DRUTOPIA_BUILD_ROOT}/findit-ci-new" ] \
  || [ -z "${FIC_CONFIG_PROJ+x}" ] \
  || [ ! -d "${FIC_CONFIG_PROJ}/sync" ]
then
  echo
  echo "Configuration error:"
  echo
  echo "  DRUTOPIA_BUILD_ROOT and/or FIC_CONFIG_PROJ not set to paths"
  echo
  echo "  Tip: try direnv: https://direnv.net/"
  echo "  See also: .envrc.example (place in project or script folder)"
  echo
  usage
  exit 1
fi
# Ready!
echo "Using build dir ${DRUTOPIA_BUILD_ROOT} and config dir ${FIC_CONFIG_PROJ}"
echo "Creating fresh tarball of sync folder..."
[ -f sync.tgz ] && rm sync.tgz
tar -c -z -f sync.tgz -C ${FIC_CONFIG_PROJ} sync
echo "Ensure installs/$1 exists on server..."
ssh findit-vm "test -d ~/installs/$1 || mkdir ~/installs/$1"
echo "Starting uploads..."
scp ${DRUTOPIA_BUILD_ROOT}/findit-ci-new.tgz findit-vm:~/installs/$1/
scp ${DRUTOPIA_BUILD_ROOT}/VERSION  findit-vm:/home/agaric/installs/$1/
scp ${DRUTOPIA_BUILD_ROOT}/install.sh findit-vm:~/installs/$1/
scp ./scripts/update.sh findit-vm:~/installs/$1/
scp sync.tgz findit-vm:~/installs/$1/
echo "Done."
