# Cambridge custom work to Featurize

Some work done directly on Find It Cambridge should be featurized into the distribution.

## Opportunity categories view

Add:

* views.view.findit_opportunity_categories
* core.entity_view_display.taxonomy_term.findit_services.teaser.yml
* core.entity_view_mode.taxonomy_term.teaser.yml

### Taxonomy term teaser

Note: This was created new.

* taxonomy_term.teaser


## Opportunities listing

Changes to not have main title be Trash:

* sync/views.view.opportunities.yml

Changes to event display:

* core.entity_view_display.node.findit_event.default.yml see for instance https://gitlab.com/find-it-program-locator/findit/issues/249#note_244889958


## Secondary footer menu

* system.menu.secondary-footer.yml


... so i stopped keeping track really a while ago, but here's a big one:


Reference blocks from within paragraphs, specifically for the Today's Events block:
https://gitlab.com/agaric/sites/find-it-cambridge-config/commit/70181e32fcd6eb625200572fccd26061e61e3bd0

# Going throug features to package up for Immigrant Family Services Institute

## Drutopia Core

Almost only fuzz from Custom Add Another junk.

Only in active config (that of course we should park somewhere else):

* core.date_format.day_of_week_month_day_year
* core.entity_form_display.taxonomy_term.tags.default and core.entity_form_display.taxonomy_term.topics.default   .. wait, how is that not in a feature somewhere already

## Find It Organization

*Is actually fine; it's just the workflow publishing that is set by actions in the other opportunities that confuses it.*


## Drutopia Landing Page

I'd need to do config actions to add the Find It search (and today's events?) paragraph(s), or just try overriding the whole content type definition in the profile.

## Drutopia Link Blocks

... nothing but durned "Custom Add Another" fuzz, did not commit.

Same with Drutopia Page.

## Find It profile

Both `node.settings` and `system.date` are "in feature missing from active config", and the former at least is causing a fatal error when trying to edit the feature:

> Notice: Undefined index: node.settings in Drupal\features_ui\Form\FeaturesEditForm->getComponentList() (line 659 of /var/www/drupal/web/modules/contrib/features/modules/features_ui/src/Form/FeaturesEditForm.php)
> Error: Call to a member function getType() on null in Drupal\features_ui\Form\FeaturesEditForm->getComponentList() (line 660 of /var/www/drupal/web/modules/contrib/features/modules/features_ui/src/Form/FeaturesEditForm.php)

But that's OK for now because less interested in writing the feature than claiming all the things it thinks it should have, for other features.


`field.field.paragraph.find_it_block.field_findit_block` should go to Drutopia Link Blocks i would think.
