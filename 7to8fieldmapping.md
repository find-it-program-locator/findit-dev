# Migration mapping

Mapping of fields in the Drupal 7 Find It Cambridge and the fields in the
Drupal 8 Find It platform to migrate their content to.


## Taxonomy terms: Neighborhood

`neighborhoods` => `findit_neighborhoods`

| Source | Target |
| ------ | ------ |
| `neighborhoods` | `findit_neighborhoods` |


## Content: Location

`location` => `findit_location`

*Note: As for all content, the label (title) can be migrated to the label (title).  In this case, though, an alternative is to re-create the autogenerated labels after migrating everything else.*

| Source | Target |
| ------ | ------ |
| `field_location_name` | `field_findit_location_name` |
| `field_address` | `field_findit_address` |
| `field_neighborhoods` | `field_findit_neighborhood` |


## Content to User: Contact

`contact` => `profile:crm_indiv`

*Note: Ideally, we would migrate one-word names from D7 contact content types to Organization (`profile:crm_org`) contacts and two-or-more word names to Individual (`profile:crm_indiv`) contacts, with the first word of the title/name going to the given name, and the second-and-later words going to the family name field.*

*An easier option would to just migrate all old contacts as Organizations, not Individuals.*

| Source | Target |
| ------ | ------ |
| `title` | `crm_name` *This is a compound name, given (first) and family (last).  See note above.* |
| `field_contact_email` | `user:email` (pretty sure) *See note below* |
| `field_contact_phone` | `crm_phone` |
| `field_contact_phone_extension` | `field_findit_crm_ext` |
| `field_contact_tty_number` | `field_findit_crm_tty_number` |
| `field_contact_role` | `field_findit_crm_role` |

*Note: Even though e-mail address is required by the CRM and ~150 of our content Contacts don't have e-mail addresses, the Contacts developers say that nothing will break if we don't have it, so at the moment i'm hoping we can at least do the migration successfully without that.  See/engage in my issue in the Contacts issue queue on their GitLab if needed.*

*General plan is to migrate `contact` nodes as [decoupled auth](https://www.drupal.org/project/decoupled_auth ) users so they will be available for a future implementation of [Contacts CRM](https://www.drupal.org/project/contacts )... but the CRM had some fields we wanted to use, so we'll have the simple CRM functionality enabled out of the gate!*


## Content: Organization

`organization` => `findit_organization`

*Note: We are splitting the `body` field into `field_findit_short_summary` and `body`.  Migrate first sentences (up to 200 characters, but only full sentences) to short summary and the remainder to body.  See [#139](https://gitlab.com/find-it-program-locator/findit/issues/139 )*

| Source | Target |
| ------ | ------ |
| `field_logo` | `field_image` |
| `body` | `field_findit_short_summary` *See note* |
| `body` | `body` *See note* |
| `field_locations` | `field_findit_locations` |
| `field_location_notes` | `field_findit_location_notes` |
| `field_always_open` | `field_findit_organization_when` *Field's content, list keys, stays the same* |
| `field_operation_hours` | `field_findit_operation_hours` |
| `field_parent_organization` | `field_findit_organization` |
| `field_organization_notes` | `field_findit_notes` |
| `field_contacts_additional_info` | `field_findit_contact_notes` |

*NOTE/CHANGE on Social Media migration: The patch to Social Media Links and Field adds a generic Blog as well a Website, correct?  We want to migrate the Tumblr field to the Blog field, because Tumblr was only ever used by City of Cambridge - Community Development Dept.(and they haven't posted there since 2017).*


## Taxonomy: Ages

Currently an Integer List.  Should it be made a Taxonomy?  I know we're adding a Senior grouping, and maybe an adult?  Basically when we have 21 == 21+ we're more in taxonomy territory than integer, but it can work both ways.  Integer is easier to index maybe?  But taxonomy is more consistent with all our others.  I vote for migrating this to taxonomy.

`whatever ages were` => `findit_ages`


## Taxonomy: Grades

`grade_eligibility_options` => `findit_grades`


## Taxonomy: Other Eligibility

`other_eligibility_options` => `findit_eligibility`


## Taxonomy terms: Accessibility

`accessibility_options` => `findit_accessibility`


## Taxonomy terms: Services

`program_categories` => `findit_services`

*Note: Not all terms will go to services; some will go to activities.  See https://gitlab.com/find-it-program-locator/findit/issues/149 for the terms to exclude and bug Clayton if the list is not there.

Also, many name changes are desired, but these can be made manually after the migrations are done if transforming while migrating isn't trivial.*


## Taxonomy terms: Activities

`program_categories` => `findit_activities`

*Note: See https://gitlab.com/find-it-program-locator/findit/issues/149 for the terms to bring to Activities.

Again, some name changes are also desired, but these can be made manually after the migrations are done if transforming while migrating isn't trivial.*




## Taxonomy terms: Amenities

`amenities` => `findit_amenities`


## Taxonomy terms: School breaks

`time_of_year` => `findit_school_breaks`

*Note: Only migrate terms 242, 232, 205 (February, April, and Winter Vacations)*


## Taxonomy terms: Special times

`time_other` => `findit_special_times`

*Rename "Early Release Days" (term 112) to "Expanded hours on early release days".*
*Drop "Snow days" (term 113).*



## Content: Program

`program` => `findit_program`

*Note: We are splitting the `body` field into `field_findit_short_summary` and `body`.  Migrate first sentences (up to 200 characters, but only full sentences) to short summary and the remainder to body.  See [#139](https://gitlab.com/find-it-program-locator/findit/issues/139 )*

| Source | Target |
| ------ | ------ |
| `field_organizations` | `field_findit_organizations` |
| `field_logo` | `field_image` |
| `field_program_categories` | `field_findit_services` *See note in taxonomy* |
| `field_amenities` | `field_findit_amenities` |
| `field_ongoing` | `field_findit_program_when` *Values stay the same.* |
| `field_program_period` | `field_findit_dates` |
| `field_time_of_year` | `field_findit_open_during` *See note in taxonomy School breaks* |
| `field_time_other` | `field_findit_special_times` *See note in taxonomy Special times* |
| `field_when_additional_info` | `field_findit_when_notes` |
| `field_registration` | `field_findit_registration_type` *See table below for value mapping* |
| `field_registration` | `field_findit_registration_when` *See table below for value mapping* |
| `field_registration_dates` | `field_findit_registration_dates` |
| `field_registration_file` | `field_findit_registration_file` |
| `field_registration_url` | `field_findit_registration_url` |
| `field_registration_instructions` | `field_findit_registration_notes` |
| `field_age_eligibility` | `field_findit_ages` |
| `field_grade_eligibility` | `field_findit_grades` |
| `field_other_eligibility` | `field_findit_eligibility` |
| `field_eligibility_notes` | `field_findit_eligibility_notes` |
| `field_reach` | `field_findit_where` *Values stay the same.* |
| `field_transportation` | `field_findit_transport_provided` *Values
stay same.* |
| `field_location_notes` | `field_findit_location_notes` |
| `field_accessibility` | `field_findit_accessibility` |
| `field_accessibility_notes` | `field_findit_accessibility_notes` |
| `field_gratis` | `field_findit_noindex_gratis` |
| `field_cost_subsidies` | `field_findit_cost_subsidies` |
| `field_cost` | `field_findit_cost_description` |
| `field_financial_aid_notes` | `field_findit_financial_aid_notes` |
| `field_financial_aid_file` | `field_findit_financial_aid_file` |
| `field_financial_aid_url` | `field_findit_financial_aid_url` |

LATE ARRIVALS:

| `field_time_day_of_week` | `field_findit_day_of_week` |
| `field_time_of_day` | `field_findit_time_of_day` |


### Value mapping for `field_registration` => `field_findit_registration_type`

| Source | Target |
| ------ | ------ |
| `not_required` | `none` |
| `ongoing` and `specific_dates` | `registration` |

### Value mapping for `field_registration` => `field_findit_registration_when`

| Source | Target |
| ------ | ------ |
| `ongoing` | `ongoing` |
| `specific_dates` | `specific_dates` |


## Content: Place

*Note: Some content currently entered as Programs should be migrated to the Place content type instead (and withheld from the program migration above).*

`program` => `findit_place`

* Only nodes in with the "Parks" term (tid 121) in the `program_categories` vocabulary.

With the available subset of fields in Place using the same mapping as documented in Program.


## Content: Event

*Note: This first set of fields is *identical* to program, with the
following removed: `field_reach`/`field_findit_where`,
`field_time_day_of_week`/`field_findit_day_of_week, `field_time_of_day`/`field_findit_time_of_day`, `field_ongoing`/`field_findit_program_when`.*

| Source | Target |
| ------ | ------ |
| `field_organizations` | `field_findit_organizations` |
| `field_logo` | `field_image` |
| `field_program_categories` | `field_findit_services` *See note in taxonomy* |
| `field_amenities` | `field_findit_amenities` |
| `field_program_period` | `field_findit_dates` |
| `field_time_of_year` | `field_findit_open_during` *See note in taxonomy School breaks* |
| `field_time_other` | `field_findit_special_times` *See note in taxonomy Special times* |
| `field_when_additional_info` | `field_findit_when_notes` |
| `field_registration` | `field_findit_registration_type` *See table below for value mapping* |
| `field_registration` | `field_findit_registration_when` *See table below for value mapping* |
| `field_registration_dates` | `field_findit_registration_dates` |
| `field_registration_file` | `field_findit_registration_file` |
| `field_registration_url` | `field_findit_registration_url` |
| `field_registration_instructions` | `field_findit_registration_notes` |
| `field_age_eligibility` | `field_findit_ages` |
| `field_grade_eligibility` | `field_findit_grades` |
| `field_other_eligibility` | `field_findit_eligibility` |
| `field_eligibility_notes` | `field_findit_eligibility_notes` |
| `field_transportation` | `field_findit_transport_provided` *Values
stay same.* |
| `field_location_notes` | `field_findit_location_notes` |
| `field_accessibility` | `field_findit_accessibility` |
| `field_accessibility_notes` | `field_findit_accessibility_notes` |
| `field_gratis` | `field_findit_noindex_gratis` |
| `field_cost_subsidies` | `field_findit_cost_subsidies` |
| `field_cost` | `field_findit_cost_description` |
| `field_financial_aid_notes` | `field_findit_financial_aid_notes` |
| `field_financial_aid_file` | `field_findit_financial_aid_file` |
| `field_financial_aid_url` | `field_findit_financial_aid_url` |

*Note: The below are new.*


| `field_programs` | `field_findit_programs` |
