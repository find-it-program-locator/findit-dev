# Integrations

## Cambridge Public Library

Term mapping - https://docs.google.com/document/d/1pyJaBaSdjh8y9okDO1Uq7tOZjR2hOtr6vfkX2sLPhpQ/edit

When a contact is not provided, an event is assigned the "Library" contact - https://www.finditcambridge.org/node/9

Library events are imported every midnight.  In `crontab -e`:

```
# Thanks to https://www.the-art-of-web.com/system/cron-set-timezone/
# This task, which runs the library import, and queue's everything for re-index, must run @ midnight.
SHELL=/bin/bash
TZ=America/New_York
0 * * * * [ "$(date +\%H\%M)" == "0000" ] && /home/agaric/bin/cronjob.sh 2>&1 > /dev/null
```

And in the referenced cronjob.sh:

```
#!/bin/bash
drush="/var/www/fic/vendor/bin/drush -r /var/www/fic/web/"
$drush migrate:import findit_library_sync_events
$drush search-api:reset-tracker main
```

To retrieve the events from LibCal's API, a `client_id` and `client_secret` which are not committed to the public repository and configuration must be included.  They are set with w

### Troubleshooting

```
"Error message: Client error: `POST https://api2.libcal.com/1.1/oauth/token` resulted in a `400 Bad Request` response:
{"error":"invalid_client","error_description":"client credentials are required"}
 at https://api2.libcal.com/1.1/events?cal_id=7052&limit=500."
```

This means that the needed `client_id` and `client_secret` credentials mentioned above were not included and need to be added to `settings.ddev.php` like this:

```php
$settings['findit_library_client_id'] = 999;
$settings['findit_library_client_secret'] = 'x9x9x9x9x9x9x9x9x9x9x9';
```

with the values retreived from the same settings on `finditvm:/var/www/fic/web/sites/default/settings.local.php`

## MIT Community

TBD
